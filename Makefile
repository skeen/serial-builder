all: release/libserial.a release/serial/serial.h

clean:
	rm -rf serial
	rm -rf build

release/serial/serial.h: serial/Makefile
	cp -r serial/include/* release/serial/

release/libserial.a: serial/Makefile
	mkdir -p build/impl/list_ports
	mkdir -p release/
	g++ serial/src/serial.cc -c -o build/serial.o -Iserial/include
	g++ serial/src/impl/unix.cc -c -o build/impl/unix.o -Iserial/include
	g++ serial/src/impl/list_ports/list_ports_linux.cc -c -o build/impl/list_ports/list_ports_linux.o -Iserial/include
	ar rvs release/libserial.a build/serial.o build/impl/unix.o build/impl/list_ports/list_ports_linux.o

serial/Makefile:
	git clone https://github.com/wjwwood/serial.git
